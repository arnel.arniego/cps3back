const express = require("express");
const router = express.Router();
const User = require("../../models/User");
const Booking = require("../../models/Booking");
const { check, validationResult } = require("express-validator");
const auth = require("../../middleware/auth");

router.post(
  "/newbooking",
  auth,
  [
    check("datetime", "Date & Time field is required")
      .not()
      .isEmpty(),
    check("service", "Service field is required")
      .not()
      .isEmpty(),
    check("stylist", "Stylist field is required")
      .not()
      .isEmpty(),
    check("address", "Address field is required")
      .not()
      .isEmpty()
  ],
  async (req, res) => {
    let errors = validationResult(req);
    if (!errors.isEmpty()) {
      var errorResponse = errors.array({ onlyFirstError: true });

      const extractedErrors = {};
      errorResponse.map(err => (extractedErrors[err.param] = err.msg));
      return res.status(400).json(extractedErrors);
    }

    const { datetime, service, stylist, address } = req.body;
    const id = req.user.id;

    try {
      let booking = new Booking({
        user: id,
        datetime,
        service,
        stylist,
        address
      });

      await booking.save();

      res.json(booking);
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server Error");
    }
  }
);

router.post(
  "/updatebooking",
  auth,
  [
    check("datetime", "Date & Time field is required")
      .not()
      .isEmpty(),
    check("service", "Service field is required")
      .not()
      .isEmpty(),
    check("stylist", "Stylist field is required")
      .not()
      .isEmpty(),
    check("address", "Address field is required")
      .not()
      .isEmpty()
  ],
  async (req, res) => {
    let errors = validationResult(req);
    if (!errors.isEmpty()) {
      var errorResponse = errors.array({ onlyFirstError: true });

      const extractedErrors = {};
      errorResponse.map(err => (extractedErrors[err.param] = err.msg));
      return res.status(400).json(extractedErrors);
    }

    const { id, datetime, service, stylist, address } = req.body;

    try {
      let booking = await Booking.findByIdAndUpdate(
        {
          _id: id
        },
        { datetime, service, stylist, address }
      );

      res.json(booking);
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server Error");
    }
  }
);

router.get("/getbookings", auth, [], async (req, res) => {
  try {
    const id = req.user.id;

    const booking = await Booking.find({ user: id });

    res.json(booking);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});

router.get("/allbookings", auth, [], async (req, res) => {
  try {
    const booking = await Booking.find({}).populate("user");

    res.json(booking);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});

router.put("/cancelbooking", [], async (req, res) => {
  try {
    const id = req.body.id;

    const booking = await Booking.findOneAndUpdate(
      { _id: id },
      { status: "cancelled" }
    );

    res.json(booking);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});

router.put("/approvebooking", [], async (req, res) => {
  try {
    const id = req.body.id;

    const booking = await Booking.findOneAndUpdate(
      { _id: id },
      { status: "approved" }
    );

    res.json(booking);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});

router.put("/rejectbooking", [], async (req, res) => {
  try {
    const id = req.body.id;

    const booking = await Booking.findOneAndUpdate(
      { _id: id },
      { status: "rejected" }
    );

    res.json(booking);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});

module.exports = router;
